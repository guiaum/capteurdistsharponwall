#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include "Id_Wifi.h"

WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(10, 0, 1, 10);     // remote IP of your computer
const unsigned int outPort = 9999;          // remote port to receive OSC
const unsigned int localPort = 8888;        // local port to listen for OSC packets (actually not used for sending)


// These constants won't change. They're used to give names to the pins used:
const int _irPin = A0;  // Analog input pin that the potentiometer is attached to
#define NB_SAMPLE 25


void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(115200);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, pass);

    while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());
  
}

void loop() {
  int ir_val[NB_SAMPLE];
  int distanceCM;
  float current;

  for (int i = 0; i < NB_SAMPLE; i++) {
    // Read analog value
    ir_val[i] = analogRead(_irPin);
  }

  // Sort it
  sort(ir_val, NB_SAMPLE);

  current = map(ir_val[NB_SAMPLE / 2], 0, 1023, 0, 3300);

  if (current < 1400 || current > 3300) {
    //false data
    distanceCM = 0;
  } else {
    distanceCM = 1.0 / (((current - 1125.0) / 1000.0) / 137.5);
  }
  
  sendOSC(distanceCM);
  
  // map it to the range of the analog out:
  // print the results to the Serial Monitor:
  Serial.print("sensor = ");
  Serial.print(distanceCM);
  Serial.println();

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(100);
}

void sort(int a[], int size) {
  for (int i = 0; i < (size - 1); i++) {
    bool flag = true;
    for (int o = 0; o < (size - (i + 1)); o++) {
      if (a[o] > a[o + 1]) {
        int t = a[o];
        a[o] = a[o + 1];
        a[o + 1] = t;
        flag = false;
      }
    }
    if (flag) break;
  }
}

void sendOSC(int _distanceCM)
{
  OSCMessage msg("/sensor5");
  msg.add(_distanceCM);
  Udp.beginPacket(outIp, outPort);
  msg.send(Udp);
  Udp.endPacket();
  msg.empty();
}
